import sys
import traceback
import logging


def initialize_logging(filename):
    logging.basicConfig(filename=filename,
                        filemode='w',
                        level=logging.DEBUG,
                        format='%(asctime)s - %(levelname)s - %(message)s',
                        )
    # set up logging to console
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(asctime)s - %(name)-12s: %(levelname)-8s %(message)s')
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)


def extract_function_name():
    tb = sys.exc_info()[-1]
    stk = traceback.extract_tb(tb, 1)
    fname = stk[0][3]
    return fname


def get_exception_message(e):
    return "Function {function_name} raised {exception_class} ({exception_docstring}): {exception_message}".format(
        function_name=extract_function_name(),  # this is optional
        exception_class=e.__class__,
        exception_docstring=e.__doc__,
        exception_message=e.message)



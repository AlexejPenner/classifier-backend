import base64
import logging
import json
import falcon
import cv2
import numpy as np
from .logging_utils import get_exception_message


class ImageService(object):
    logger = logging.getLogger(__name__)

    def __init__(self, config):
        self.config = config
        self.image_storage_path = self.config["paths"]["images"]

    # def on_post(self, req, resp):
    #     result = {}
    #     try:
    #         req_body = req.get_param('image')
    #         raw = req_body.file.read().decode('utf-8')
    #         jsn = json.loads(raw)
    #         try:
    #             encoded_data = jsn['data'].split(',')[1]
    #             img = self.readb64(encoded_data)
    #             cv2.imwrite(f"{jsn['uuid']}.jpg", img)
    #         except FileNotFoundError as e:
    #             self.logger.error("No image found for uuid {}".format(uuid))
    #         resp.body = json.dumps(result)
    #     except Exception as e:
    #         self.logger.exception(e)
    #         resp.status = falcon.HTTP_500

    def on_post(self, req, resp):
        result = {},
        try:
            req_body = req.stream.read()
            try:
                img = self.readb64(req_body.decode('utf-8'))
            except binascii.Error as e:
                resp.status = falcon.HTTP_500

            cv2.imwrite("image.jpg", img)
            result = [
                {"label": "LL6400",
                 "positionToTopLeft": {"xCoordinate": "0", "yCoordinate": "1"},
                 "positionToTopRight": {"xCoordinate": "0", "yCoordinate": "1"},
                 "positionToBottomLeft": {"xCoordinate": "0", "yCoordinate": "1"},
                 "positionToBottomRight": {"xCoordinate": "0", "yCoordinate": "1"}
                 },
                {"label": "LL4242",
                 "positionToTopLeft": {"xCoordinate": "0", "yCoordinate": "1"},
                 "positionToTopRight": {"xCoordinate": "0", "yCoordinate": "1"},
                 "positionToBottomLeft": {"xCoordinate": "0", "yCoordinate": "1"},
                 "positionToBottomRight": {"xCoordinate": "0", "yCoordinate": "1"}
                }
            ]
            resp.body = json.dumps(result)
        except ValueError as e:
            self.logger.exception(e)
            resp.status = falcon.HTTP_500

    def readb64(self, base64_string):
        nparr = np.fromstring(base64.b64decode(base64_string), np.uint8)
        img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
        return img



from keras.applications.inception_v3 import InceptionV3
from keras.optimizers import SGD
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras.preprocessing.image import ImageDataGenerator
from keras.layers import Input, Flatten, Dense
from keras.models import Model
from time import time
import os

def train(input_shape=(224, 224, 3), batch_size=16, epochs=50):
        train_data_dir = './training_data'

        input_tensor = Input(shape=input_shape)

        # Load pre-trained Model
        base_model = InceptionV3(
                include_top=False,
                weights='imagenet',
                input_tensor=input_tensor
        )

        for layer in base_model.layers:
            layer.trainable = True

        x = Flatten(name='flatten')(base_model.output)
        x = Dense(2048, activation='relu', name='fully_connected_1')(x)
        x = Dense(7, activation='softmax', name='predictions')(x)

        model = Model(inputs=base_model.input, outputs=x, name='classifier')

        model.compile(loss='binary_crossentropy',
                      optimizer=SGD(lr=1e-4, momentum=0.9),
                      metrics=['accuracy'])

        train_datagen = ImageDataGenerator(
                rescale=1./255,
                shear_range=0.2,
                zoom_range=0.2,
                horizontal_flip=True
        )

        # test_datagen = ImageDataGenerator(rescale=1./255)

        train_generator = train_datagen.flow_from_directory(
                train_data_dir,
                target_size=(224, 224),
                batch_size=batch_size,
                class_mode='categorical'
        )

        # validation_generator = test_datagen.flow_from_directory(
        #         validation_data_dir,
        #         target_size=(224, 224),
        #         batch_size=batch_size,
        #         class_mode='binary')

        model.summary()

        checkpoint = ModelCheckpoint(
                filepath=os.path.join('./weights', 'weights{loss:.4f}.hdf5'),
                monitor='loss',
                verbose=1,
                save_best_only=True,
                mode='min')

        tensorboard = TensorBoard(log_dir="logs/{}".format(time()))

        callbacks_list = [checkpoint, tensorboard]

        # fine-tune the model
        model.fit_generator(
                train_generator,
                steps_per_epoch=128,
                epochs=epochs
        )


if __name__ == '__main__':
        input_shape = (224, 224, 3)
        train(input_shape=input_shape, batch_size=16, epochs=50)



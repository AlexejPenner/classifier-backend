import cv2
import numpy as np
import matplotlib.pyplot as plt


img = cv2.imread('/home/alexej/ifco/ifco-classification/image1.jpg', 0)

print(img.shape)

# cv2.imshow('image', img)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

f = np.fft.fft2(img)
fshift = np.fft.fftshift(f)
magnitude_spectrum = 20*np.log(np.abs(fshift))

plt.subplot(121), plt.imshow(img, cmap='gray')
plt.title('Input Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122), plt.imshow(magnitude_spectrum, cmap='gray')
plt.title('Magnitude Spectrum'), plt.xticks([]), plt.yticks([])
plt.show()

img_hist = np.sum(img, axis=1)/img.shape[0]
hist = np.sum(magnitude_spectrum, axis=1)/magnitude_spectrum.shape[0]

plt.subplot(121),
plt.bar(np.arange(img_hist.shape[0]), img_hist)
plt.subplot(122),
plt.bar(np.arange(-int(hist.shape[0]/2), int(hist.shape[0]/2)+1), hist)
plt.show()





#
# rows, cols = img.shape
# crow, ccol = int(rows/2), int(cols/2)
# fshift[crow-30:crow+30, ccol-30:ccol+30] = 0
# f_ishift = np.fft.ifftshift(fshift)
# img_back = np.fft.ifft2(f_ishift)
# img_back = np.abs(img_back)
#
# plt.subplot(131), plt.imshow(img, cmap='gray')
# plt.title('Input Image'), plt.xticks([]), plt.yticks([])
# plt.subplot(132), plt.imshow(img_back, cmap='gray')
# plt.title('Image after HPF'), plt.xticks([]), plt.yticks([])
# plt.subplot(133), plt.imshow(img_back)
# plt.title('Result in JET'), plt.xticks([]), plt.yticks([])
#
# plt.show()
#
# # converting to gray scale
# gray = img
#
# # remove noise
# img = cv2.GaussianBlur(gray, (3, 3), 0)

# # convolute with proper kernels
# laplacian = cv2.Laplacian(img, cv2.CV_64F)
# sobelx = cv2.Sobel(img, cv2.CV_64F, 1, 0, ksize=5)  # x
# sobely = cv2.Sobel(img, cv2.CV_64F, 0, 1, ksize=5)  # y
#
# plt.subplot(2, 2, 1), plt.imshow(img, cmap='gray')
# plt.title('Original'), plt.xticks([]), plt.yticks([])
# plt.subplot(2, 2, 2), plt.imshow(laplacian, cmap='gray')
# plt.title('Laplacian'), plt.xticks([]), plt.yticks([])
# plt.subplot(2, 2, 3), plt.imshow(sobelx, cmap='gray')
# plt.title('Sobel X'), plt.xticks([]), plt.yticks([])
# plt.subplot(2, 2, 4), plt.imshow(sobely, cmap='gray')
# plt.title('Sobel Y'), plt.xticks([]), plt.yticks([])
#
# plt.show()
#
# f = np.fft.fft2(sobely)
# fshift = np.fft.fftshift(f)
# magnitude_spectrum = 20*np.log(np.abs(fshift))
#
# plt.subplot(121), plt.imshow(sobely, cmap='gray')
# plt.title('Input Image'), plt.xticks([]), plt.yticks([])
# plt.subplot(122), plt.imshow(magnitude_spectrum, cmap='gray')
# plt.title('Magnitude Spectrum'), plt.xticks([]), plt.yticks([])
# plt.show()

import logging
import falcon
from falcon_multipart.middleware import MultipartMiddleware
import os
import sys
import configparser as cp
from waitress import serve
from utils.logging_utils import initialize_logging
from utils.image_tool import ImageService


def init_service(config_parser):
    initialize_logging("app.log")
    initialize_logging(logging.DEBUG)

    app = falcon.API(middleware=[MultipartMiddleware()])
    app.add_route("/images", ImageService(config=config))
    logger = logging.getLogger(__name__)
    logger.info("eForce21 MNIST running")
    return app


def start(config_parser):
    return init_service(config_parser)


if __name__ == "__main__":
    execution_mode = "prod"
    if len(sys.argv) > 1:
        if sys.argv[1] == "--execution_mode=dev":
            execution_mode = "dev"

    config = cp.ConfigParser()
    app_path = os.path.dirname(__file__)
    if execution_mode == "dev":
        config.read(os.path.join(app_path, "config.dev.ini"))
    else:
        config.read(os.path.join(app_path, "config.ini"))

    # Set common file paths
    config.add_section("paths")
    config.set("paths", "images", os.path.join(app_path, "..", "images"))

    hostname = config["hosting"]["hostname"]
    port = int(config["hosting"]["port"])

    # create folders if they don't exist
    if not os.path.exists(config["paths"]["images"]):
        os.makedirs(config["paths"]["images"])

    serve(start(config), host=hostname, port=port)
